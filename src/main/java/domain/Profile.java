package main.java.domain;

public class Profile {
	
	private int id;
	
	private String username;
	private int age;
	private int weight;
	private String eyes;
	private String hair;
	private String address;
	private String education;
	private String description;
	
	public Profile(){
		
	}
	
	
	public Profile(String username, int age, int weight, String eyes, String hair, String address, String education, String description){
		this.username = username;
		this.age = age;
		this.weight = weight;
		this.eyes = eyes;
		this.hair = hair;
		this.address = address;
		this.education = education;
		this.description = description;
	}
	
	
	public Profile(int id, String username, int age, int weight, String eyes, String hair, String address, String education, String description){
		this.id = id;
		this.username = username;
		this.age = age;
		this.weight = weight;
		this.eyes = eyes;
		this.hair = hair;
		this.address = address;
		this.education = education;
		this.description = description;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public String getEyes() {
		return eyes;
	}
	public void setEyes(String eyes) {
		this.eyes = eyes;
	}
	public String getHair() {
		return hair;
	}
	public void setHair(String hair) {
		this.hair = hair;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	

}
