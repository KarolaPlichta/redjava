package main.java.rest.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import main.java.dao.IRepositoryCatalog;
import main.java.dao.RepositoryCatalog;
import main.java.domain.Profile;
import main.java.rest.dto.ProfileDto;

@Path("profile")
public class ProfileService {
	
	IRepositoryCatalog repository = new RepositoryCatalog();
	int currentId = 0;
	
	@PUT
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response add(ProfileDto dto){
		
		Profile profile = new Profile();
		profile.setId(currentId);
		profile.setAddress(dto.getAddress());
		profile.setAge(dto.getAge());
		profile.setDescription(dto.getDescription());
		profile.setEducation(dto.getEducation());
		profile.setEyes(dto.getEyes());
		profile.setHair(dto.getHair());
		profile.setUsername(dto.getUsername());
		profile.setWeight(dto.getWeight());
		repository.profiles().add(profile);
		
		currentId ++;
		return Response.status(201).build();
	}
	
	@GET
	@Path("/{personId}")
	@Produces("application/json")
	public Profile get(@PathParam("personId") int id){
		Profile profile = repository.profiles().get(id);
		if (profile == null) {
			return new Profile();
		}
		return profile;
	}	
	
	@DELETE
	@Path("/")
	@Produces("application/json")
	public Response delete(@PathParam("personId") int id, ProfileDto dto){
		Profile profile = new Profile();
		profile.setId(id);
		profile.setAddress(dto.getAddress());
		profile.setAge(dto.getAge());
		profile.setDescription(dto.getDescription());
		profile.setEducation(dto.getEducation());
		profile.setEyes(dto.getEyes());
		profile.setHair(dto.getHair());
		profile.setUsername(dto.getUsername());
		profile.setWeight(dto.getWeight());
		repository.profiles().delete(profile);
		return Response.status(200).build();
	}	
	
	@GET
	@Path("/all")
	@Produces("application/json")
	public List<Profile> getAll(){
		return repository.profiles().getAll();
	}	

}
