package main.java.dao;

import main.java.domain.Profile;

public class RepositoryCatalog implements IRepositoryCatalog{

	@Override
	public IRepository<Profile> profiles() {
		return new ProfileRepository();
	}

}
