package main.java.dao;

import main.java.domain.Profile;

public interface IRepositoryCatalog {
	
	public IRepository<Profile> profiles();

}
