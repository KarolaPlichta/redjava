package main.java.dao;

import java.util.ArrayList;
import java.util.List;

import main.java.domain.Profile;

public class ProfileRepository implements IRepository<Profile> {

	private static List<Profile> profiles = new ArrayList<Profile>();
	private int currentId=1;
	
	@Override
	public void add(Profile entity) {
		entity.setId(currentId);
		profiles.add(entity);
		currentId++;
		
	}

	@Override
	public void delete(Profile entity) {
		profiles.remove(entity);
	}

	@Override
	public Profile get(int id) {
		for(Profile current : profiles){
			if(current.getId()==id) return current;
		}
		return null;
	}

	@Override
	public List<Profile> getAll() {
		return profiles;
	}

}
