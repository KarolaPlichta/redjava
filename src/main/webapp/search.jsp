<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>RedJava</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="view.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="header">
	<div id="logo">
		<h1><a href="#">RedJava</a></h1>
	</div>
	<div id="menu">
		<ul>
			<li class="active"><a href="viewProfile.jsp">Twój profil</a></li>
			<li><a href="friends.jsp">Znajomi</a></li>
			<li><a href="visitors.jsp">Odwiedzający</a></li>
			<li><a href="logout.jsp">Wyloguj</a></li>
		</ul>
	</div>
</div>
<hr />

<div id="wrapper">
	<div id="page">
		<!-- start content -->
		<div id="content">
		
		<b>Opcje wyszukiwania:</b> <br>
		<form name="search" action="Search" method="POST">
		<ul><li>wiek od: <input type="text" name="ageform">, do: <input type="text" name="ageto"></li>
		<li>wykształcenie<select name="education">
  				<option value="podstawowe">podstawowe</option>
 				<option value="średnie">średnie</option>
  				<option value="wyższe">wyższe</option>
			</select></li>
		<li>waga od: <input type="text" name="weightform">, do: <input type="text" name="weightto"></li>
		<li>skąd: <input type=text name="address"></li>
		</ul>
		<input type="submit" name="search" value="Wyszukaj">
		</form>
		
		
</div></div>
</div>		

<div id="footer">
<p id="legal"> 2007 Uncomplicated . All Rights Reserved.
	
</div>
</body>
</html>