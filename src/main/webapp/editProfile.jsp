<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Prosty serwis randkowy</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="view.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!-- start header -->
<div id="header">
	<div id="logo">
		<h1><a href="#">RedJava</a></h1>
	</div>
	<div id="menu">
		<ul>
			<li class="active"><a href="viewProfile.jsp">Twój profil</a></li>
			<li><a href="firends.jsp">Znajomi</a></li>
			<li><a href="visitors.jsp">Odwiedzający</a></li>
			<li><a href="search.jsp">Wyszukiwarka</a></li>
			<li><a href="logout.jsp">Wyloguj</a></li>
		</ul>
	</div>
</div>
<hr />
<!-- end header -->
<!-- start page -->
<div id="wrapper">
	<div id="page">
		<!-- start content -->
		<div id="content">
		<form  name="editProfile" action= "EditProfile" method="POST">
			<ul>
			<li>zdjęcie: <input type="file" name="photo" value="Dodaj zdjęcie"></li>
			<li>wiek: <input type=text name="age" required> </li>
			<li>waga: <input type=text name="weight"></li>
			<li>kolor oczu: <input type=text name="eyes"></li>
			<li>kolor włosów: <input type=text name="hair"> </li>
			<li>zamieszkanie: <input type=text name="address"></li>
			<li>wykształcenie<select name="education">
  				<option value="podstawowe">podstawowe</option>
 				<option value="średnie">średnie</option>
  				<option value="wyższe">wyższe</option>
			</select></li></ul> 
			opis: <br>
			<textarea name="description"></textarea>
			<br>
			<input type="submit" name="wyslij" value="Zapisz zmiany">
			<input type="reset" name="rezygnuj" value="zrezygnuj">
			
			</form>
		
</div></div></div>
<!-- end page -->
<!-- start footer -->
<div id="footer">
<p id="legal">2015 RedJava . All Rights Reserved.

<!-- end footer -->
</div>
</body>
</html>
